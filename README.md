# PageRank


[The PageRank Citation Ranking: Bringing Order to the Web](http://ilpubs.stanford.edu/422/1/1999-66.pdf)
January 29, 1998

> In order to measure the relative importance of web pages, we propose
> PageRank, a method for computing a ranking for every web page based on the
> graph of the web


```mermaid
graph TD;
  A-->C;
  B-->C;
```

> Figure 1: A and B are Backlinks of C

> We give the following intuitive description of PageRank: a page has high
 rank if the sum of the ranks of its backlinks is high[...]

> Let $`F_u`$ be the set of pages $`u`$ points to[...]

> Let $`N_u = \left|F_u\right|`$ be the number of links from $`u`$ and let
> $`c`$ be a factor used for normalization[...]

> $`R`$ [...] is a sligthly simplified version of PageRank.[...]

> Let $`A`$ be a square matrix with the rows and column corresponding to web
> pages.

> Let $`A_{u,v} = \frac 1{N_u}`$ if there is an edge from $`u`$ to $`v`$ and
> $`A_{u,v}=0`$ if not.

> If we treat $`R`$ as a vector over web pages, then we have
> $`R=cAR`$. So $`R`$ is an eigenvector of $`A`$ with eigenvalue $`c`$.